import chai from "chai";
import chaiHttp from "chai-http";
import { createExpressApp, parseRequestPayload, calc } from "../src/server";

chai.should();
chai.use(chaiHttp);

export const mochaHooks = async () => {
  const app = createExpressApp();
  parseRequestPayload(app);
  calc(app);

  return {
    beforeAll() {
      this.requester = chai.request(app).keepOpen();
    },

    afterAll() {
      this.requester.close();
    },
  };
};
