describe("calculator endpoints", function () {
  it("does basic addition", async function () {
    const response = await this.requester
      .post("/calc")
      .send({ operandA: 2, operandB: 2, operator: "+" });

    response.body.should.have.property("message", 4);
  });
});
