import {
  createExpressApp,
  enableAccesslogs,
  parseRequestPayload,
  calc,
  startServer,
} from "./server";

const app = createExpressApp();
enableAccesslogs(app);
parseRequestPayload(app);
calc(app);
startServer(app);
