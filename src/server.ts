import express, { Application, Request, Response } from "express";
import morgan from "morgan";
import bodyParser from "body-parser";

export function createExpressApp(): Application {
  return express();
}

export function enableAccesslogs(app: Application): void {
  app.use(morgan("dev"));
}

export function parseRequestPayload(app: Application): void {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
}

function returnMessage(res: Response, code: number, message: string | number) {
  if (code !== 200) {
    return res.status(code).send({ error: message });
  }
  return res.status(code).send({ message });
}

export function calc(app: Application): void {
  app.post("/calc", async (req: Request, res: Response): Promise<Response> => {
    if (!req.body?.operandA || !req.body?.operandB || !req.body?.operator) {
      return returnMessage(res, 400, "Bad Request");
    }

    let { operandA, operandB } = req.body;
    const { operator } = req.body;

    if (typeof operandA === "string") {
      operandA = parseInt(operandA, 10);

      if (Number.isNaN(operandA)) {
        return returnMessage(res, 400, "operandA is not a number");
      }
    }

    if (typeof operandB === "string") {
      operandB = parseInt(operandB, 10);

      if (Number.isNaN(operandB)) {
        return returnMessage(res, 400, "operandB is not a number");
      }
    }

    if (!["+", "-", "/", "x"].includes(operator)) {
      return returnMessage(res, 400, "Operator is not valid math operator");
    }

    let mathResult: number;

    switch (operator) {
      case "+":
        mathResult = operandA + operandB;
        break;
      case "-":
        mathResult = operandA - operandB;
        break;
      case "/":
        mathResult = operandA / operandB;
        break;
      case "x":
        mathResult = operandA * operandB;
        break;
      default:
        break;
    }

    return returnMessage(
      res,
      200,
      // eslint-disable-next-line prettier/prettier
      mathResult
    );
  });
}

export function startServer(app: Application): void {
  const PORT = 3000;

  try {
    app.listen(PORT, (): void => {
      console.log(`Connected successfully on port ${PORT}`);
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (error: any) {
    console.error(`Error occured: ${error.message ?? error}`);
  }
}
